<?php
get_header();
?>

	<main id="primary" class="site-main wrap with-sidebar">

		<div class="singular">
			<?php
			if ( have_posts() ) : 

			while ( have_posts() ) : the_post();

				get_template_part( 'templates/content', get_post_type() );

				endwhile;

				the_posts_navigation();

			else :

				echo '<p>Default content</p>';

			endif;

			if( get_post_type() === 'post' ) 
				comments_template();
			?>
		</div>

		<?php
		get_sidebar();
		?>

	</main><!-- #main -->

<?php
get_footer();
