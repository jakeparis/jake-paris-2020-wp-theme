



<footer class="pre-footer">
	<div class="wrap">
		<?php dynamic_sidebar('pre-footer'); ?>
	</div>
</footer>

<footer class="page-footer">
	<div class="wrap">

		<div class="footer-contact">
			<p>
				<b>Jake Paris</b><br>
				Lewiston, Maine<br>
				Copyright <?php echo date('Y') ?>
			</p>
		</div>

		<?php get_template_part('templates/snippet', 'social-links' ); ?>

	</div>
</footer>


		<?php 
		wp_footer();
		?>
	</body>
</html>
