
<?php

ob_start();
if( is_front_page() 
	&&
	is_active_sidebar( 'home')
) {
	dynamic_sidebar( 'home' );
}

elseif( 
	get_post_type() == 'page'
) {
	dynamic_sidebar( 'pages' );
}

elseif(
	get_post_type() == 'post'
) {
	dynamic_sidebar( 'posts' );
}

$sidebar = ob_get_contents();

ob_end_clean();

if( ! $sidebar )
	return;
?>

<aside class="right-bar">
	<?= $sidebar ?>
</aside>