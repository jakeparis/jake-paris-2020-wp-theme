
<div <?php post_class('content-template') ?>>

	<h1 class="page-title">
		<?php the_title() ?>
	</h1>

	<div class="content-area">
		<?php the_content() ?>
	</div>

</div>
