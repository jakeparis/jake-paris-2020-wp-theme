<div class="outbound-links">
	
	<a class="outbound-link _bandlab" title="Jake on BandLab" href="https://www.bandlab.com/jakepaint" target="_blank" id="outbound-bandlad">
		<?= file_get_contents( get_stylesheet_directory() . '/img/logo-bandlab.svg' ); ?> 
	</a>

	<a class="outbound-link _linkedin" title="Jake on LinkedIn" href="https://www.linkedin.com/in/jakeparis/" target="_blank" id="outbound-linkedin">
		<?= file_get_contents( get_stylesheet_directory() . '/img/logo-linkedin.svg' ); ?> 
	</a>

	<a class="outbound-link _gitlab" title="Jake on GitLab" href="https://gitlab.com/jakeparis/" target="_blank" id="outbound-gitlab">
		<?= file_get_contents( get_stylesheet_directory() . '/img/logo-gitlab.svg' ); ?> 
	</a>

	<a class="outbound-link _so" title="Jake on Stack Overflow" href="https://stackoverflow.com/users/362769/jakeparis" target="_blank" id="outbound-stackoverflow">
		<?= file_get_contents( get_stylesheet_directory() . '/img/logo-stackoverflow.svg' ); ?>
	</a>

	<a class="outbound-link _telegram" title="Jake on Telegram" href="https://t.me/jakeparis" target="_blank" id="outbound-telegram">
		<?= file_get_contents( get_stylesheet_directory() . '/img/logo-telegram.svg' ); ?> 
	</a>
	
</div>