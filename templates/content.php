<?php 
/*
This is the default template content if no other is used
*/
?>

<div <?php post_class('content-template') ?>>

	<div class="post-meta">
		<?php the_date( 'M, Y' ) ?>
		&mdash;
		by <?php the_author() ?>
	</div>

	<h1 class="page-title">
		<?php the_title() ?>
	</h1>

	<div class="content-area">
		<?php the_content() ?>
	</div>

	<div class="tax">
		<div class="_categories">
			<p>Category &mdash;</p>
			<?php the_category('') ?>
		</div>

		<div class="_tags">
			<?php the_tags('Tags &mdash;', '', '') ?>
		</div>
		
	</div>

</div>
