<?php 
/*
This is the default template content if no other is used
*/
?>

<div class="list-item">

	<h3>
		<a href="<?php the_permalink() ?>">
			<?php the_title() ?>
		</a>
	</h3>

	<div class="post-meta">
		<?php the_date( 'M, Y' ) ?>
	</div>

	<div class="content-area excerpt">
		<?php the_excerpt() ?>
	</div>

	<div class="tax">
		<?php the_tags('Tags &mdash;', '', '') ?>
	</div>


</div>

<div class="list-item-image">

	<?php the_post_thumbnail( ) ?>

</div>