<?php
define('JAKEPARIS_THEME_VERSION', '1.4.5');

define('JAKEPARIS_THEME_URL', get_stylesheet_directory_uri() );

// init so that blocks can use them
add_action('init', function(){

	if( ! is_admin() ) :

		$fonts = 'family=Fira+Code:wght@300&amp;family=Fira+Sans:ital,wght@0,100;0,200;0,400;0,700;0,900;1,100;1,200;1,400;1,700';
		wp_enqueue_style('jakeparis/fonts',
			"https://fonts.googleapis.com/css2?{$fonts}&amp;display=swap",
			[],
			'1'
		);

		wp_enqueue_style(
			'highlight-default',
			JAKEPARIS_THEME_URL . '/lib/highlight/styles/shades-of-purple.css',
			[],
			'10.3.1'
		);
		wp_enqueue_script(
			'highlight',
			JAKEPARIS_THEME_URL . '/lib/highlight/highlight.pack.js',
			[],
			'10.3.1'
		);

		// wp_enqueue_script(
		// 	'instantpage',
		// 	JAKEPARIS_THEME_URL . '/lib/instantpage.js',
		// 	[],
		// 	'5.1.0',
		// 	true
		// );

		wp_enqueue_style('jakeparis/base', 
			JAKEPARIS_THEME_URL . '/style.css',
			[], 
			JAKEPARIS_THEME_VERSION
		);

		wp_enqueue_style('jakeparis',
			JAKEPARIS_THEME_URL . '/assets/main.css',
			[
				'jakeparis/base',
				'jakeparis/fonts',
				'highlight-default',
				'wp-block-library',
			],
			JAKEPARIS_THEME_VERSION
		);

		wp_enqueue_script('jakeparis',
			JAKEPARIS_THEME_URL . '/assets/main.js',
			[
				'highlight',
			],
			JAKEPARIS_THEME_VERSION
		);

	endif;

	/* Things to enqueue both in public & admin
	 */
	wp_enqueue_style( 'jakeparis/blocks-public', 
		JAKEPARIS_THEME_URL . '/assets/style-index.css', 
		[
			// make sure ours comes after
			'wp-block-library',
		],
		JAKEPARIS_THEME_VERSION
	);
	

});

add_action('wp_head', function(){

	foreach([
		JAKEPARIS_THEME_URL . '/style.css',
		JAKEPARIS_THEME_URL . '/assets/main.css',
	] as $url ) {
		$url .= '?ver=' . JAKEPARIS_THEME_VERSION;
		echo '<link rel="preload" as="style" href="'.$url.'">' . "\n";
	}

});

add_action('after_setup_theme', function(){

	add_theme_support('post-thumbnails');
	add_theme_support('html5');
	add_theme_support('title-tag');
	// add_theme_support( 'wp-block-styles' );
	add_theme_support('align-wide');

	add_theme_support('custom-header',array(
		'height' => 400,
		'width' => 1400,
		//'default' => '',
		'uploads' => true
	));

	register_nav_menu( 'main', 'Main Menu' );
	
	register_sidebar([
		'name' => 'Single Pages',
		'id' => 'pages',
		'description' => 'The sidebar on single pages',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
	]);
	register_sidebar([
		'name' => 'Home',
		'id' => 'home',
		'description' => 'The sidebar on the home page',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
	]);
	register_sidebar([
		'name' => 'Single Posts',
		'id' => 'posts',
		'description' => 'The sidebar on single posts',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
	]);
	register_sidebar([
		'name' => 'Pre Footer',
		'id' => 'pre-footer',
		'description' => 'The pre-footer area',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
	]);
	

});

require_once 'blocks.php';


function get_jp_excerpt($wordcount=50){
	// global $post;
	$content = strip_tags( get_the_excerpt() );
	$contentArr = explode(' ',$content,$wordcount);

	if(count($contentArr) >= $wordcount) {
		array_pop($contentArr);
		$last = $wordcount-2;
		$contentArr[ $last ] = preg_replace('/[^a-zA-Z0-9 ]/', '', $contentArr[$last]);	
		$content = implode(' ', $contentArr);
		$content = trim($content);
		$content .= '&hellip;';
	}
	return $content;
}
function jp_excerpt($wordcount=50){
	echo get_jp_excerpt($wordcount);
}


add_filter('script_loader_tag', function($tag, $handle) {

	$handlesToModify = [];
	/**
	 * filter which allows adding attributes to script tags output with wp_enqueue_script
	 * 
	 * @param   $handlesToModify  an array with script handle as key, and the value is an
	 *                            array with attribute => value pairs. For an attribute with
	 *                            no value (like <input disabled>), leave the value empty or false 
	 */
	$handlesToModify = apply_filters('jp_enqueue_script_attribute_mod', $handlesToModify);
	if( array_key_exists($handle, $handlesToModify) && is_array($handlesToModify[$handle]) ) {

		foreach($handlesToModify[$handle] as $key=>$val) {

			if( strpos($val, "'") !== false ) {
				$val = str_replace("'",'"', $val);
			}

			// if we just want a non-value attribute like async and it's not already in there
			if( empty($val) && strpos($tag, $key) === false ){
				$tag = str_replace('<script ', "<script $key ", $tag);
			} else {

				// if the attribute is already in the tag, update it's value with our value
				if( strpos($tag, " $key=") !== false){
					$tag = preg_replace("_ {$key}=[\"|'][^\"']*[\"|'] _", " {$key}='{$val}' ", $tag);
				} else {

					// otherwise, just add our attribute="value" to the tag
					$tag = str_replace('<script ', "<script $key='{$val}' ", $tag);
				}
			}
		}
	}

	return $tag;

}, 15, 2);

add_filter('style_loader_tag', function($tag, $handle) {

	$handlesToModify = [];
	/**
	 * filter which allows adding attributes to css link tags output with wp_enqueue_script
	 * 
	 * @param   $handlesToModify  an array with script handle as key, and the value is an
	 *                            array with attribute => value pairs. For an attribute with
	 *                            no value (like <input disabled>), leave the value empty or false 
	 */
	$handlesToModify = apply_filters('jp_enqueue_style_attribute_mod', $handlesToModify);
	if( array_key_exists($handle, $handlesToModify) && is_array($handlesToModify[$handle]) ) {

		foreach($handlesToModify[$handle] as $key=>$val) {

			if( strpos($val, "'") !== false ) {
				$val = str_replace("'", '"', $val);
			}

			// if we just want a non-value attribute like async and it's not already in there
			if( empty($val) && strpos($tag, $key) === false ){
				$tag = str_replace('<link ', "<link $key ", $tag);
			} else {

				// if the attribute is already in the tag, update it's value with our value
				if( strpos($tag, " $key=") !== false){
					$tag = preg_replace("_ {$key}=[\"|'][^\"']*[\"|'] _", " {$key}='{$val}' ", $tag);
				} else {

					// otherwise, just add our attribute="value" to the tag
					$tag = str_replace('<link ', "<link $key='{$val}' ", $tag);
				}
			}
		}
	}

	return $tag;

}, 15, 2);

// add_filter('jp_enqueue_script_attribute_mod', function( $handles ){

// 	foreach([
// 		'jakeparis',
// 	] as $slug) {
// 		$handles[ $slug ] = [
// 			'async' => '',
// 			'defer' => '',
// 		];
// 	}

// 	return $handles;
// });

add_filter('jp_enqueue_style_attribute_mod', function( $handles ) {
	foreach( [
		// 'jakeparis/blocks-public',
		'highlight-default',
		// 'jakeparis/fonts',
	] as $slug ) {
		$handles[ $slug ] = [
			'media' => 'print',
			'onload' => 'this.media="all"',
		];
	}
	return $handles;
});


/**
 * Updater
 */
require_once get_stylesheet_directory() . '/updater/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/jakeparis/jake-paris-2020-wp-theme',
	__FILE__, //Full path to the main plugin file or functions.php.
	'jakeparis2020'
);
