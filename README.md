# Jake Paris 2020 WordPress Theme

# Changelog

### 1.3.2

Added updater

### 1.3.0

Updated footer outbound links
- removed keybase
- added telegram & bandlab
- cleanedup css

### 1.1.2
Fix bug in where highlight causing js error. The library
can't be defer/async because my script relies on it. 

### 1.1.x
I don't know. 


### 1.0.x
Release