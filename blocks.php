<?php

add_action('after_setup_theme', function(){
	
	add_theme_support('editor-color-palette', [
		[
			'name' => 'My Green',
			'slug' => 'my-green',
			'color' => 'var(--color_Green)',
		],[
			'name' => 'Burnt Pink',
			'slug' => 'burnt-pink',
			'color' => 'var(--color_BurntPink)',
		],[
			'name' => 'Hot Pink',
			'slug' => 'hot-pink',
			'color' => 'var(--color_HotPink)',
		],[
			'name' => 'Light Gray',
			'slug' => 'light-gray',
			'color' => 'var(--color_LightGray)',
		],[
			'name' => 'Dark Blue',
			'slug' => 'dark-blue',
			'color' => 'var(--color_DarkBlue)',
		],[
			'name' => 'Bright Tan',
			'slug' => 'bright-tan',
			'color' => 'var(--color_BrightTan)',
		],
	]);

	add_theme_support( 'editor-styles');
	add_editor_style( 'assets/main.css' );	
});


add_action('init', function(){

	// register_block_type( 'jakeparis/block', [

	// ]);
	
});

add_action('enqueue_block_editor_assets', function(){

	wp_enqueue_script('jakeparis/blocks', 
		JAKEPARIS_THEME_URL . '/assets/index.js',
		[],
		JAKEPARIS_THEME_VERSION
	);
	wp_enqueue_style( 'jakeparis/blocks', 
		JAKEPARIS_THEME_URL . '/assets/index.css', 
		[
			'jakeparis/blocks-public',
		],
		JAKEPARIS_THEME_VERSION
	);

});