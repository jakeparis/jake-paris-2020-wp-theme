<?php
defined('ABSPATH') || die();
get_header();
?>

	<main id="primary" class="site-main wrap">

		<h1 class="page-title">
		<?php
		if( is_search() )
			echo "Searched for " . get_search_query();
		elseif( is_singular() )
			the_title();
		elseif( is_home() )
			echo "Latests Posts";
		elseif( is_post_type_archive( 'bates_events' ))
			echo $sitename . " Events";
		elseif( is_tag() )
			echo "Tagged: " . single_tag_title('',0);
		elseif( is_category() )
			echo "Category: " . single_cat_title('',0);
		elseif( is_day() )
			echo "Archives: " . get_the_date("F j, Y");
		elseif( is_month() )
			echo "Archives: " . get_the_date("F Y");
		elseif( is_year() )
			echo "Archives: " . get_the_date("Y");
		elseif( is_archive() )
			echo "Archives";
		?>
		</h1>

		<div class="post-list">
			<?php
			if ( have_posts() ) : 

			while ( have_posts() ) : the_post();

				get_template_part( 'templates/content', 'in-list' );

				endwhile;

				the_posts_navigation([
					'prev_text' => 'older',
					'next_text' => 'newer',
				]);

			else :

				echo '<h1 class="page-title">Error</h1>
				<p>Try again?</p>';

			endif;
			?>
		</div>
	</main><!-- #main -->

<?php
// get_sidebar();
get_footer();
