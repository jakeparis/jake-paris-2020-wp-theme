<?php
get_header();
?>

	<main id="primary" class="site-main wrap with-sidebar">

		<div class="singular content-template">

			<h1 class="page-title">Searched for <?= get_search_query() ?></h1>

		
				<?php if ( have_posts() ) : ?>
					<div class="post-list">

					<?php while ( have_posts() ) : the_post();

						get_template_part( 'templates/content', 'in-list' );

						endwhile;

						the_posts_navigation([
							'prev_text' => 'older',
							'next_text' => 'newer',
						]);

						?>

					</div>

				<?php else :

					echo '<h2>No matches</h2>';

				endif;
				?>

				
				<hr>

				<p>Wordpress&rsquo; search isn't all that great. Try searching <a href="https://duckduckgo.com/?q=site%3A<?= site_url() ?>+<?= urlencode(the_search_query()) ?>">searching with a search engine</a> instead. </p>
				
			</div>

		</div>
	</main><!-- #main -->

<?php
// get_sidebar();
get_footer();
