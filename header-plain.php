<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="description" content="<?php bloginfo('description') ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<?php
		wp_head();

		// styles here is me being really lazy
		?>
		<style>
			header.main-header {
				min-height: 200px;
			}
			h1.page-title {
				font-weight: 600;
				margin-top: 1em;
			}
			body.plain .content-template .page-title {
				display: none;
			}
			nav.main-menu {
				display: block; 
			}
			.main-menu .back-to-jp-link {
				margin-top: 0;
				padding-top: 0.5em;
				font-size: 16px;
				font-weight: 600;
			}
		</style>
	</head>
	<body <?php body_class('plain') ?> >
		<?php wp_body_open() ?>	

		<header class="main-header">

			<nav class="main-menu wrap">
				<p class="back-to-jp-link">
					<a href="<?= home_url() ?>">&laquo; back to JakeParis.com</a>
				</p>

				<h1 class="page-title">
					<?php the_title() ?>
				</h1>
			</nav>
		
		</header>
