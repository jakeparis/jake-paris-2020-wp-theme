<?php
get_header();
?>

	<main id="primary" class="site-main wrap with-sidebar">

		<div class="singular">

			<h1 class="page-title">Error (404): Missing Page</h1>
					
			<p>Page/post missing. Try a search?</p>

			<?php get_search_form( ); ?>

		</div>

	</main>

<?php
// get_sidebar();
get_footer();
