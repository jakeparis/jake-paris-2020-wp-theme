<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="description" content="<?php bloginfo('description') ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<?php
		wp_head();
		
		/* Use this to be the featured image in the header
		*/
		if( is_singular() ) {
			$tn = get_the_post_thumbnail_url( null, 'large' );
			$featuredImage = $headerClassExtra = null;
			if( $tn ) { 
				$headerClassExtra = 'with-image';
				$bgPosition = get_post_meta(get_the_ID(),'background-position',true);
				if( $bgPosition )
					$bgPosition = "background-position: {$bgPosition};";

				$featuredImage = '<div id="featured-image"></div>';
				?>
				<style>
					#featured-image {
						background-image:  url(<?=$tn?>);
						<?= $bgPosition ?>
					}
				</style>
				<?php
			}
		}
		/**/
		?>
	</head>
	<body <?php body_class() ?> data-instant-intensity="mousedown">
		<?php wp_body_open() ?>	

		<header class="main-header <?= $headerClassExtra ?>">

			<h1 class="site-name wrap">
				<a href="<?= home_url() ?>">
					<?php bloginfo('sitename') ?>
				</a>
			</h1>

			<?= $featuredImage ?>

		</header>

		<?php
		wp_nav_menu( [
			'theme_location'  => 'main',
			'container'       => 'nav',
			'container_class' => 'main-menu',
			'fallback_cb'     => false,
			// 'before'          => '',
			// 'after'           => '',
			// 'link_before'     => '',
			// 'link_after'      => '',
		]);
